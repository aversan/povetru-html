import 'svgxuse';
import ismobile from 'ismobilejs';
import autosize from 'autosize';
import 'popper.js';
import 'bootstrap-select/js/bootstrap-select';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/button';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/dropdown';
import Swiper from 'swiper';
import 'uikit';

global.jQuery = $;
global.$ = $;

$.fn.selectpicker.Constructor.DEFAULTS.dropupAuto = false;

$(() => {
  // 60fps scrolling using pointer-events: none
  // https://habrahabr.ru/post/204238/

  const { body } = document;
  let timer;

  window.addEventListener('scroll', () => {
    clearTimeout(timer);
    if (!body.classList.contains('disable-hover')) {
      body.classList.add('disable-hover');
    }
    timer = setTimeout(() => {
      body.classList.remove('disable-hover');
    }, 500);
  }, false);

  if (ismobile.phone) {
    body.classList.add('is-mobile');
  } else if (ismobile.tablet) {
    body.classList.add('is-tablet');
  } else {
    body.classList.add('is-desktop');
  }


  // drop

  // (function () {
  //   const _Drop = Drop.createContext({
  //     classPrefix: 'drop',
  //   });
  //
  //   const setup = function () {
  //     return $('.js-drop').each(function () {
  //       const $dropwrap = $(this);
  //       const theme = $dropwrap.data('theme');
  //       const position = $dropwrap.data('position');
  //       const openOn = $dropwrap.data('open-on') || '';
  //       const $target = $dropwrap.find('.drop-target');
  //       $target.addClass(theme);
  //       const content = $dropwrap.find('.drop-content').html();
  //
  //       const drop = new _Drop({
  //         target: $target[0],
  //         classes: theme,
  //         position,
  //         constrainToWindow: false,
  //         constrainToScrollParent: false,
  //         openOn,
  //         content,
  //         hoverOpenDelay: 1000,
  //         // remove: true
  //       });
  //
  //       $(this)[0].drop = drop;
  //
  //       return drop;
  //     });
  //   };
  //
  //   const init = function () {
  //     return setup();
  //   };
  //
  //   init();
  // }).call(this);


  // dropdown

  $(document)
    .on('click.bs.dropdown.data-api', '.dropdown-menu.noclose', (e) => {
      e.stopPropagation();
    });

  $(document).on('click.bs.dropdown.data-api', '[data-dismiss="dropdown"]', function () {
    $(this).parents('.dropdown').eq(0).find('[data-toggle="dropdown"]')
      .dropdown('toggle');
  });

  $(document).on('click', '.js-radio-buttons .btn, .js-radio-buttons .js-btn',  function () {
    $(this).siblings().removeClass('active').end().addClass('active');
    $(this).parent('[class*="col-"]').siblings().find('.js-btn').removeClass('active').end().end().addClass('active');
  });


  // autosize textarea

  autosize($('textarea.js-autosize'));


  $('.js-indeterminate').prop('indeterminate', true);

  $('.js-selectpicker').selectpicker({
    //dropupAuto: false,
  });


  // swiper

  let swiperScrollbar = new Swiper('.js-swiper-container-scrollbar', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: false,
    },
  });

  let swiperScrollbarTable = new Swiper('.js-swiper-container-scrollbar-table', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    mousewheel: true,
    freeMode: true,
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: false,
    },
    grabCursor: false,
  });

  let swiperScrollbarRequestCourier = new Swiper('.js-swiper-container-scrollbar-request-courier', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    mousewheel: false,
    freeMode: true,
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: true,
    },
    grabCursor: false,
  });

  let swiperSlidableMenu = new Swiper('.js-swiper-container-slidable-menu', {
    slidesPerView: 'auto',
    initialSlide: 1,
    resistanceRatio: 0,
    slideToClickedSlide: false,
    breakpoints: {
      991.98: {
        slidesPerView: '2',
        initialSlide: 1,
      },
    },
  });

  // menu toggle

  $(document).on('click', '.js-menu-toggle',  function () {
    if ($(this).hasClass('hide-sidebar')) {
      $('.js-menu-toggle').add('.header-navbar').add('.main-container').add('.sidebar').removeClass('hide-sidebar');
      $(body).addClass('disable-scroll');
    } else {
      $('.js-menu-toggle').add('.header-navbar').add('.main-container').add('.sidebar').addClass('hide-sidebar');
      $(body).removeClass('disable-scroll');
    }
  });


  // card collapse

  $(document)
    .on('show.bs.collapse', '.card-collapse',  function () {
      $(this).parent('.card').addClass('is-expanded');
    })
    .on('hide.bs.collapse', '.card-collapse',  function () {
      $(this).parent('.card').removeClass('is-expanded');
    });

  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  // We listen to the resize event
  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });
});
