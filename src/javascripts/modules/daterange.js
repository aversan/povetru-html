import moment from 'moment';
import 'daterangepicker';

export default class DateRange {
  constructor(el) {
    this.el = el;

    const separator = ' - ';
    const dateFormat = 'DD.MM.YY';

    const options = {
      autoUpdateInput: false,
      autoApply: false,
      locale: {
        format: dateFormat,
        separator: separator,
        'applyLabel': 'Применить',
        'cancelLabel': 'Отменить',
        'fromLabel': 'С ',
        'toLabel': 'По ',
        'daysOfWeek': [
          'Вс',
          'Пн',
          'Вт',
          'Ср',
          'Чт',
          'Пт',
          'Сб',
        ],
        'monthNames': [
          'Январь',
          'Февраль',
          'Март',
          'Апрель',
          'Май',
          'Июнь',
          'Июль',
          'Август',
          'Сентябрь',
          'Октябрь',
          'Ноябрь',
          'Декабрь',
        ],
        'firstDay': 1,
      },
      minDate: moment().add(1, 'days'),
      maxDate: moment().add(359, 'days'),
      opens: 'center',
      buttonClasses: 'btn',
      applyButtonClasses: 'btn-primary',
      cancelButtonClasses: 'btn-outline-primary',
    };

    $(this.el).each(function(el) {
      const $group = $(this);
      const $controlStart = $group.find('.form-control.start');
      const $controlEnd = $group.find('.form-control.end');
      const parentEl = $group.attr('data-parentEl') || '';
      let formatStartDate = '';
      let formatEndDate = '';

      $controlStart.add($controlEnd)
        .daterangepicker({
          ...options,
          parentEl,
        })
        .on('apply.daterangepicker' ,function(event, picker) {
          const isStart = this.className.match(/start/g) != null;
          formatStartDate = picker.startDate.format(dateFormat);
          formatEndDate = picker.endDate.format(dateFormat);

          $controlStart.val(formatStartDate);
          $controlEnd.val(formatEndDate);

          if (isStart) {
            $controlEnd.data('daterangepicker').setStartDate(formatStartDate);
            $controlEnd.data('daterangepicker').setEndDate(formatEndDate);
          } else {
            $controlStart.data('daterangepicker').setStartDate(formatStartDate);
            $controlStart.data('daterangepicker').setEndDate(formatEndDate);
          }
        });
    });
  }
}
