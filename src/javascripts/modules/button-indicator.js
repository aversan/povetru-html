export default class ButtonIndicator {
  constructor(el) {
    this.el = el;

    $(this.el).each(function() {
      const $group = $(this);
      const $buttons = $group.children('.btn');
      const $marker = $group.find('.active-marker');

      $buttons.on('click',  function() {
        let $activeButton = $(this);
        ButtonIndicator.moveMarker($activeButton, $marker, $buttons);
      });

      $(window).resize(function(){
        ButtonIndicator.moveMarker($activeButton, $marker, $buttons);
      });
    });
  }

  static anchorWidthCounter($buttons) {
    let anchorWidths = 0;
    let buttonWidth;
    let buttonPadLeft;
    let buttonPadRight;
    let buttonTotalWidth;

    $buttons.each(function(index, elem) {
      let $button = $(elem);
      let activeTest = $button.hasClass('active');

      if(activeTest) {
        return false;
      }

      buttonWidth = $button.width();
      buttonPadLeft = parseFloat($button.css('padding-left'));
      buttonPadRight = parseFloat($button.css('padding-right'));
      buttonTotalWidth = buttonWidth + buttonPadLeft + buttonPadRight;

      anchorWidths = anchorWidths + buttonTotalWidth;
    });

    return anchorWidths;
  }

  static moveMarker($activeButton, $marker, $buttons) {
    let activeWidth = $activeButton.width();
    let activePadLeft = parseFloat($activeButton.css('padding-left'));
    let activePadRight = parseFloat($activeButton.css('padding-right'));
    let totalWidth = activeWidth + activePadLeft + activePadRight;

    let precedingAnchorWidth = ButtonIndicator.anchorWidthCounter($buttons);

    $marker.css('display','block');
    $marker.css('width', totalWidth);
    $marker.css('left', precedingAnchorWidth);
  }
}
